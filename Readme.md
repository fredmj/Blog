# Un CMS pour FMJConsulting!

````bash
  motivation=on
  if [[ $work="great" and fun="on" ]]; then
    return success
    else try(again)
  fi
````

**FMJConsulting** fait peau neuve et souhaite utiliser un nouveau CMS*(Content Management System). C'est l'occasion de découvrir ce que propose la communauté en matière de gestionnaire de contenu. Les technologies du web et des applications mobiles sont au coeur de la grande majorité des activités digitales. Un jolie pretexte pour s'amuser avec Node.JS, Laravel et consort.

Ceci est donc le premier article du nouvel emballage. Mais les possibilités offertes par les dernires technologies web sont riches et complexes. Il ne me suffira pas d'un simple mois d'août pour en faire le tour. Surtout si vous avez comme moi laissé s'accumuler une dette technologique connsequente sur le sujet. Nous allons donc faire le tri en usant de méthode et de machines virtuelles. Ainsi une première liste se constituera en se “bornant” aux deux (ou trois) premières pages de mon ami Google et aux liens secondaires glanés sur les sites consultés par rebond[^1]. Le trie s'effectuera selon un mode opératoire que nous allons définir.

Aujourd'hui, le marché des CMS est accaparé par WordPress, Joomla et Drupal. Il existe également des CMS spécialisés pour la vente de produits en ligne, comme prestashop ou shopify. Ces outils sont d'excellents choix pour ceux souhaitant se concentrer sur
le coeur de leur activité sans se soucier du contenu technologique sous jaccent. Or en la matière, ma démarche se rapproche plus d'un architecte Web que d'un marchand ou d'un journaliste. Donc j'écarte à priori ces outils.

Il me faut cependant du responsive, du HTML5, du Javascript, du PHP7 côté serveur. Il me faut de la sécurité pour les bases de données et du filtrage utilisateurs. Il faut qu’il soit beau et facile à lire avec des snippets de codes et des simili-captures d’écran. Je veux des gists et même des plugins pour construire des pages directement a partir de gists. Bref! Je recherche quelque chose d’un peu “technique”, actuel, profitant de technos jeunes, mais simples, robustes, efficaces, ouvertes si qui n'exclut pas le payant.

Mais par dessus tout il faut que ce soit rapide. J'écris mon texte depuis l'éditeur `Atom`, je le copie/colle dans mon CMS, Et Voila!

Allez c'est parti.


## Mode Opératoire : Définir le besoin, le caractériser par des critères évaluables, construire une matrice de compatibilité pour l'étude des solutions, conclure.

### Définir le besoin réel

* Présenter fmjconsulting.fr, ses partenaires, ses collaborateurs et son actualité.
* Un blog pour évoquer des points techniques, écrire des billets d'humeur.
* Un espace pour essayer des applications.

### Se référer à des caractéristiques quantifiable

Les critères retenus pour pour aider à l'évaluation objective du produit sont :
* la cohérence, la richesse et l'exactitude de la documentation
* les conditions d'installation sur une CentOS 7
* la facilité d'administration des utilisateurs, des pages et des layouts
* la capacité à rédiger les billets en Markdown
* la facilité d'application des thèmes
* la sécurité liée aux utilisateurs
* la vitalité du projet
* la licence opensource, l'accessibilité aux sources et l'esprit communautaire
* l'esthétique global du site
* enfin le potentiel, l'ouverture et la fiabilité technologique des outils
sous jaccent aux fonctionnement de l'application : robustesse du framework, ghist, DSL ouverts, etc

Avec dix points par critère sachant que certains sont éliminatoires (l'installation et la sécurité)
cela nous permet de définir un score sur 10.

Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10

La démarche ici sera toute simple et rapide. Nous allons utiliser un master virtualbox
que nous déclinerons en fonction des téléchargements des sources github.

    virtualbox 5.26
    CentOS 7.3
    Apache
    Nginx
    MariaDB
    MongoDB

Puis nous y ajouterons

    Node.JS v6 | Node.JS v8 avec npm(@latest)
    PHP v5 | PHP v7 avec composer(@latest)

Enfin nous y grefferons quelques extenssions et au besoin Java, Python, Ruby ...
Alors nous devrions avoir tout ce dont nous avons besoin.

Ensuite il convient pour effectuer nos tests :
* d'écrire deux types de contenu
  * Un post type "Lorem ipsum ..." utilisant toutes les fonctionnalités rédactionnelles du markdown language.
  * Un autre représentant un cas concret, à savoir cet article lui-même.
* Définir un (ou deux) layout(s) jugés utile pour recevoir les futurs articles.

Il va falloir mettre la main à la patte.
* Du javascript côté serveur? écrire du code!
* travailler les css via un pre-processeur, less.js par exemple? compiler!
* une mise en page (layout) via des viewers comme pug? DSL!

Si vous êtes comme moi un tantinet allergique au DSL[¹], ca ne va pas être facile.

Donc pour résumer grossierement les technologies :
* Coté serveur
  * Javascript
      * nodejs
      * npm
      * strongloop
  * Php
      * Laravel
      * composer artisan
  * CSS
    * Less
    * SaSS
    * Pug
  * gulp
  * grunt
* Coté client
  * Javascript
      * AngularJS
      * AngularJS
      * ReactJS
      * View.JS
* HTML5

### Réduire sa dette technologique, mais raison garder.

C’est jeune, c’est beau, c’est rapide, mais maitriser ces technologies cela reste un métier.
Et je ne vais pas m’improviser Web Developper du jour au lendemain.
Ceci dit, les sites web à la bon papa (LAMP, Flash, JBOSS, Tomcat) c'est fini.
Et il va falloir que je prenne le temps de bousculer mes vieux reflexes.
Dépoussiérer tout ça va me faire le plus grand bien.
Les applications Web et Mobile sont ludiques et souvent bluffantes.
Mais il faut également se méfier du syndrome “tout nouveau, tout beau”.

*  RIP
  *  *Hatch.js*,
  *  *callipso*,
  *  *pencilblue*,
  *  *cody.js*,
  *  *scotch*,
  *  *InvaNode*,
  *  *etc* ...


Sans tomber dans le mélodrame ni forcément viser le standard de demain,
ce serait pas mal que l’espérance de vie dépasse les 3 ans.


## Etudes

### Node.JS
Le framework javascript fork en Ayo à l'heure ou j'écris es lignes.
13 machine testées.

```
[fredmj@MSI vboxlib]$ ./vboxlistgroup.sh | grep "/Lab CMS/CentOS Lab NodeJS CMS" | cat -n
     1	NAME=name="CENTOS 7 ExpressJS";GROUP=groups="/Lab CMS/CentOS Lab NodeJS CMS"
     2	NAME=name="CENTOS 7 KeystoneJS";GROUP=groups="/Lab CMS/CentOS Lab NodeJS CMS"
     3	NAME=name="CENTOS 7 Cody";GROUP=groups="/Lab CMS/CentOS Lab NodeJS CMS"
     4	NAME=name="CENTOS 7 ghost";GROUP=groups="/Lab CMS/CentOS Lab NodeJS CMS"
     5	NAME=name="CENTOS 7 Enduro.JS";GROUP=groups="/Lab CMS/CentOS Lab NodeJS CMS"
     6	NAME=name="CENTOS 7 Apostrophe2";GROUP=groups="/Lab CMS/CentOS Lab NodeJS CMS"
     7	NAME=name="CENTOS 7 Total.JS";GROUP=groups="/Lab CMS/CentOS Lab NodeJS CMS"
     8	NAME=name="CENTOS 7 HEXO";GROUP=groups="/Lab CMS/CentOS Lab NodeJS CMS"
     9	NAME=name="CENTOS 7 taracot";GROUP=groups="/Lab CMS/CentOS Lab NodeJS CMS"
    10	NAME=name="CENTOS 7 bucket.io";GROUP=groups="/Lab CMS/CentOS Lab NodeJS CMS"
    11	NAME=name="CENTOS 7 Assemble";GROUP=groups="/Lab CMS/CentOS Lab NodeJS CMS"
    12	NAME=name="CENTOS 7 NodeJSv8";GROUP=groups="/Lab CMS/CentOS Lab NodeJS CMS"
    13	NAME=name="CENTOS 7 Node.JS v6";GROUP=groups="/Lab CMS/CentOS Lab NodeJS CMS"

```
##### Express.
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
C’est un framework Node.JS pas un CMS. Il est très ouvert, efficace, assez complet, robuste
et surtout rapide. Tout ce que j’aime. Moderne, il est en passe de devenir un standard.
Nous sommes ici essentiellement dans le monde MVC et le but affiché
est l’assistance à la création d’application. Essayer les fonctionnalités
rentre dans le cadre d’essais orientés NodeJS et application Javascript [⁴].
our un autre billet donc.
Ceci dit et pour se faire une idée, les premiers pas du guide de mise en route
du site expressjs.com sont plutot bien fait. Mais attention c’est (très) addictif.
Vous serez prévenu!

##### KeystoneJS
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
C'est LE cms du framework Express. Minimaliste, robuste, large communauté.
Très bien.
Peut-être d’une philosophie généraliste et moins orienté DevOps.
Mais pour le moment j’aime. Il faut pratiquer le pug (ex jade) et le less.js.
Mais aussi Handlebars, Grunt.
Ce qui signifie : encore des DSL.
Mais cela s'accommode bien de l’esprit “hacking” et ça c’est me plait.

##### ghost
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Riche en technologies misent en oeuvre :
  * Node.js
  * Amber.JS
  * Handlebar.js
  * npm/Yarn.

Mais la documentation se contredit souvent et n’est pas vraiment à jour.
Du coup quelques inquiétudes sur la maintenabilité de l’application socle.
Le projet github est très actif mais très orienté Ubuntu/Nginx. Officiellement CentOS n'est
pas supporté même s'il est tout fait possible d'y faire fonctionner Ghost.

##### enduro.js
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10

Une installation à l’opposé de ghost: Simple et efficace.
Simple comme minimalistic ?! Oui car tout le reste est à faire pour obtenir
l'équivalent du même ghost.
Mais c’est intéressant si le temps et l’envie de s’y mettre est à votre disposition.
Handlebars et Sass pour la mise en place des css qui vont bien, et Hop! le tour est joué.
J’ai même vu des modules Angular.js. Il manque peut-être quelques thèmes.

##### Apostrophe2
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Une installation plutôt simple, même si la documentation est orienté OSX.
Une “philosophie” de construction très orienté développement. Pour ajouter du contenu à la
page home il est necessaire d'éditer `lib/modules/apostrophe-pages/views/pages/home.html` qui
hérite des propriétés de `lib/modules/apostrophe-templates/views/outerLayout.html` pour y ajouter
ce que l'on souhaite. La documentation propose :
````
{{ apos.area(data.page, 'body', {
      widgets: {
        'apostrophe-images': {
          size: 'full'
        },
        'apostrophe-rich-text': {
          toolbar: [ 'Styles', 'Bold', 'Italic', 'Link', 'Unlink' ],
          styles: [
            { name: 'Heading', element: 'h3' },
            { name: 'Subheading', element: 'h4' },
            { name: 'Paragraph', element: 'p' }
          ]
        }
      }
    })
}}
````

Un outil fiable et une doc complète. Parfait pour progresser avec l'environnement JS.
Un peu juste pour servir de CMS out-of-the-box.

##### Total.js
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Une mise en route ultra simple et du code pour la suite.
Un vrai framework! Tout est offert dès le début. Mais ce n’est pas un CMS.
La documentation est bien faite et le framework est solide.
Et pour ceux et celles qui aiment coder, le bonheur est au bout des doigts.
Ideal pour apprendre.

##### Hexo
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Quelques petites difficultés d’installation mais suffisamment d’informations
disponible pour contourner le problème de droits.
Le projet est actif grace à NoahDragon [https://github.com/NoahDragon].
C’est un Framework NodeJS orienté blog, ce qui correspond assez bien à mon besoin.
Beaucoup de thèmes, utilise le langage markdown et s’avère sobre, efficace et rapide.
Il manque peut-être un plugin type administration des  pages et des posts.
En tout cas, j’aime beaucoup.

##### Taracot
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Un framework à priori destiné à beaucoup de choses: commerce, blog, réseaux sociaux…
Une techno MongoDB et Redis, orienté utilisateurs OSX.
Quelques difficultés techniques avec l’étape node install-system et
pas assez de temps pour que je puisse aboutir à une installation sur CentOS 7.
Mais bon, le sujet commence apparemment à dater sur github.

##### Bucket.io
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Simple et rapide graçe à mongodb, mais RIPed depuis 2015.
Un simple npm install buckets --save-dev bloque sur une bonne cinquantaine d’erreurs.
Il me faut surement utiliser plus en avant `grunt` et les `grunt-plugins` pour aboutir.
Mais quelque chose me dit que ce projet est R.I.P. Dommage, je manque de temps.

##### Assemble
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Outils 'As-code'. Documentation d'une qualité légèrement en retrait par rapport aux ténors JS.
Mais grace au travail de Jon Schlinkert [https://github.com/jonschlinkert] il existe
tout un tas de boilerplates(5) disponnible pour illustrer des templates.
Peut-etre pour une prochaine génération de FMJConsulting.

##### Ember.JS
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Excellente documentation. Elle permet d'aborder le framework simplement mais avec une
courbe d'apprentissage à forte pente. C'est ludique, puissant et efficace. La manipulation
des pages, routes, templates crées se fait à la main. Mais la generation et réutilisabilité
des composants est particulièrement bien assisté par le client `ember-cli`.
A noter que Ember.JS utilise l'excellent watchman.
Un standard.


### Laravel.
18 machines testées pour le framework Php
```
[fredmj@MSI vboxlib]$ ./vboxlistgroup.sh | grep "/Lab CMS/CentOS Lab PHP CMS" | cat -n
1	NAME=name="CENTOS 7 Croogo";GROUP=groups="/Lab CMS/CentOS Lab PHP CMS"
2	NAME=name="CENTOS 7 Subrion";GROUP=groups="/Lab CMS/CentOS Lab PHP CMS"
3	NAME=name="CENTOS 7 TYPO3";GROUP=groups="/Lab CMS/CentOS Lab PHP CMS"
4	NAME=name="CENTOS 7 Concrete5";GROUP=groups="/Lab CMS/CentOS Lab PHP CMS"
5	NAME=name="CENTOS 7 apache HTML5Up";GROUP=groups="/Lab CMS/CentOS Lab PHP CMS"
6	NAME=name="CENTOS 7 Grav";GROUP=groups="/Lab CMS/CentOS Lab PHP CMS"
7	NAME=name="CENTOS 7 CMS MadeSimple";GROUP=groups="/Lab CMS/CentOS Lab PHP CMS"
8	NAME=name="CENTOS 7 SPIP";GROUP=groups="/Lab CMS/CentOS Lab PHP CMS"
9	NAME=name="CENTOS 7 ZWII";GROUP=groups="/Lab CMS/CentOS Lab PHP CMS"
10	NAME=name="CENTOS 7 AasgardCMS";GROUP=groups="/Lab CMS/CentOS Lab PHP CMS"
11	NAME=name="CENTOS 7 PHP7/HTTPD2 bare";GROUP=groups="/Lab CMS/CentOS Lab PHP CMS"
12	NAME=name="CENTOS 7 October";GROUP=groups="/Lab CMS/CentOS Lab PHP CMS"
13	NAME=name="CENTOS 7 BootstrapCMS";GROUP=groups="/Lab CMS/CentOS Lab PHP CMS"
14	NAME=name="CENTOS 7 Lavalite";GROUP=groups="/Lab CMS/CentOS Lab PHP CMS"
15	NAME=name="CENTOS 7 quarx";GROUP=groups="/Lab CMS/CentOS Lab PHP CMS"
16	NAME=name="CENTOS 7 Laravel";GROUP=groups="/Lab CMS/CentOS Lab PHP CMS"
17	NAME=name="CENTOS 7 PyroCMS";GROUP=groups="/Lab CMS/CentOS Lab PHP CMS"
18	NAME=name="CENTOS 7 TypiCMS";GROUP=groups="/Lab CMS/CentOS Lab PHP CMS"
```

##### Laravel
Laravel, Homestead, Vagrant.
Un framework PHP vraiment riche et actuel qui se partage le marché avec Node.JS.
Déjà un standard.

##### CakePHP
Un excellent framework, ancien, robuste qui s'accompagne d'une documentation claire et
complète.

##### Croogo
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
La documentation d'installation est sommaire, puisqu'il s'agit d'un lien vers le
Readme.md du depot github. S'appuie sur CakePHP il manque quelques thèmes.
Les plugins (administration, themes, taxonomie, ...) simplifient pas mal l'administration
du site. Pas très convaincant pour un "vieux" CMS.

##### Subrion
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Une documentation correcte. Facile à installer et assez complet. Plutôt orienté shop CMS.
Certains plugins/template sont payant.
Pas de support natif pour la rédaction en Markdown. Des themes (templates) parfait pour
créer une agence dédié à l'automobile. Un plugin quasi parfait pour l'administration.
Un bon support pour créer out-of-the-box une application web commercante.

##### TYPO3
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Un CMS capable de prendre en charge toutes les attentes. Une documentation complète.
Une interface d'administration riche et complète. Un esprit PAO dans la rédaction des pages.
Il s'agit d'une application très pro.
Il manque la prise en charge des markdown pour cette version v9.0. pour atteindre la perfection.
Attention tout de même à la stabilité du cache et aux écritures en base de données
qui peuvent jouer des tours lors de phase de restauration/sauvegarde.


##### Concrete5
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Une documentation relativement complète, tant pour l'utilisateur que pour le développeur.
La gestion du contenu est bien assuré par une nouvelle version du dashboard. La mise en
page est simple et efficace. Drag and drop de blocs, choix de mock-up, c'est presque ludique.
Pas mal de plugins et thèmes mais beaucoup sont payants. Les prix sont abordable.
Néenmoins, sans acheter le plugin pour la rédaction en Markdown (commonMark)
je ne peux pas essayer correctement mes pages templates.

##### Html5up
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Juste des templates, du CSS et du talent. Un design simple et épuré.
Cela nous donne toute la mesure de ce que nous avons réellement besoin.
Bluffant.

##### Grav
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Aboutie.
Très simple à installer, enfin lorsque l’on connaît les deux trois manipulations
systématiques à faire. Efficace et esthétiquement sobre.
Des skeletons, des thèmes et des plugins de qualité.
La documentation contrairement à beaucoup d’autres est correcte et
explique partout la même chose [²].
Un standard pour moi.

##### CMSMadeSimple
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Des sources disponnibles via ... subversion. Et un repo github mise à jour il y ... 7 ans.
Un produit d'une acienne génération qui tente de resister vaillament aux assauts du temps.
Des plugins et des thèmes additionnelles honorables, mais le module d'administration est
un peu viellisant. Pas d'éditeur compatible markdown (à la place il y a tinyMCE)
ni de gestionnaire de theme. Ceci dit le theme simplex est vraiment de qualité.
Enfin l'insertion de snippet est problèmatique, dès qu'il y a un couple '{}'.
Ceci qui est éliminatoire pour moi.
Une autre source d'interrogation réside dans la partie installation qui se présente sous
forme d'un binaire et pas d'un script clairement lisible. Juste par soucis d'encapsuler
le .phar (ce qui est expliqué sur le site
"Q: Why is there a .php file inside the archive, and not a .phar?
A: Most web servers are not configured to treat files ending with .phar as executable PHP scripts.
 Therefore we have renamed the file as a .php file so that web servers will know
 to execute the script. Please note however, that this is a binary file and must
 be treated as such. )"
Après avoir lu la documentation et avec un peu de HTML5/CSS,  cmsms devient
malgré tout un choix crédible.


##### SPIP
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Apache, Php 7 et Mysql (ou sqlite3). C’est simple, c’est sobrement esthetique,
et c’est fonctionnel dès la première installation.
De plus c’est ancien! Donc robuste. Ancien et Moderne : un mariage passionnant.
La prise en main est **Ultra** rapide. Je n’avais pas fini d’installer SPIP
que je commencais à rédiger la première page, installer mon logo, etc.
La documentation existe dans toutes les langues, et est aussi complète que clair.
Par contre la rédaction des billets se fait via le modèle de SPIP et il
n'est pas question ici de Markdown. Un plugin existe
mais il ne semble pas compatible avec la version 3.2 (issue #5).

##### Zwii
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Un de ceux que j’aurais aimé essayer mais l'installation me laisse sur
une page vierge. Le forum répond à mes questions, mais tout le monde est en
vaccances. Comme moi. Dommage, pour une prochaine fois car semble intéressant.


##### AasgardCMS
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Une documentation fournie et bien faite. Une rédaction en Markdown nativement prise en charge.
Un module d'administration complet, à ceci pret que je n'ai pas reussi à y configurer le menu.
Par contre le projet est actif et productif. Le temps de la rédaction de cet artcile asgardcms
est passé de la version 2 a la version 3 pour prendre en charge laravel5.
La communauté sous Github et `#Slack` sont aussi très actif (si vous êtes polie et précis).

##### October
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Laravel ici aussi. La documentation semble complète et cohérente,
le chan IRC est assez actif,les sources sont sur Github
et le projet fonctionne (cf [issue#3038](]https://github.com/octobercms/october/issues/3038).

##### Quarx
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Ne s’installe pas correctement sous ma CentOS7 (issue #89).
La documentation manque de cohérence, et n'est pas très claire.
Mais le projet est  assez actif et le tout me semble porter un beau potentiel.

##### PyroCMS
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Le top pour Laravel. Installation rapide et documentation parfaitement en phase.
Riche en fonctionnalité, prise en main aisée et sobriété esthétique au rendez-vous.
Mais la version pro est payante, donc attention. Ceci dit il est tellement
efficace que j'a directement continué la rédaction de l'article après l'installation.
J'ai récupérer ce que j'avais écrit pour l'ajouter à Atom.

##### Bootstrap CMS
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
La documentation est succinte et s'adresse plutôt aux initiés. Du coup le risque de
faire des aller/retour dans les fichiers de configuration mais c'est en forgeant...
Ainsi par exemple, ne pas  oublier l'étape `php artisan key:generate` a placer
dans `config/app.php`.

##### TypiCMS
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Sur la base de Laravel 5, s'installe assez facilement
(Mariadb, nodejsv8, composer, npm, php7.1 et quelques plugins).

##### CoasterCMS
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Facile à installer, Facile à administrer. Rapide, responsive et ouvert.
Très complet dans ses fonctionnalités d'où la suspicion d'une charge d'administration
peut-être plus forte que certains autres. Les layout utilise le blade.php engine de laravel.
La documentation est de bonne qualité et permet de passer rapidement du coté dev.
Parfait pour une petite équipe de 2 à 5 personnes.
Un excellent CMS, en passe de devenir un standard.

##### Lavalite
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Ne s'installe pas correctement sur ma CentOS7 avec un Laravel opérationel.
La documentation est trop succinte.

##### PageKit
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Ne s'installe pas correctement sous ma CentOS7.

##### BotbleCMS
Doc | Install | Admin | Pages | Markdown | Themes | Secu |  Sources | Projet | Techno
----|---------|-------|-------|----------|--------|------|----------|--------|-------
d/10|  i/10   |  a/10 |  p/10 |    m/10  |   t/10 |  s/10|    s/10  |   p/10 |  t/10
Payant.

## Et les Headless CMS ?
Se concentrer sur le contenu et laisser le reste dans les nuages. Pile dans la mouvance du moment.
Un peu de cloud et un peu de local. Je ne m’embête plus avec l’administration technique du blog (IT + socle)
 et je ne m’intéresse qu’aux articles. Si comme moi vous aimez le Cloud et la virtualisation, que vous utilisez viagrant, aws, openstack
et que la plupart de vos données sont hebergés par Google/Github, et que vous vous interessez plus
au contenu qu'a l'aspect FullStack, alors vous serez certainement séduit par ces solutions.

# Conclusion
J'ai l'impression d'avoir sous estimé la tache qui m'attendais.
Inutile de vous expliquer que le monde des applications web risque de donner une
bonne claque a beaucoup de vieux quinquagénaires dans mon genre.
Donc naturellement, lorsqu’un dinosaure comme moi décide de refaire son site web
il hume le doux parfum de l’extinction des espèces.
Mais comprenons nous, je trouve cela plutôt motivant.
Les logiciels sont aujourd’hui orientées web et mobiles. Ils sont rapides,
UX friendly et technologiquement agnostiques.
Ils tiennent en leurs seins des promesses  d’uniformisation jadis rêvé
par JAVA. Ces applications reposent de moins en moins sur l’antique modèle
3 tiers monolithique. La diversités des langages explose,
ils sont de plus en plus spécifiques à des contextes d’exploitation (DSL).
L’architecture logiciel de ces frameworks s’appuient sur des solutions
facilement virtualisables et aisément hébergeable dans le cloud.
Il reste évidemment quelques lieux construit sur le
modèle standard (serveurs physiques, applications compilées embarquées,
client lourds, architectures monolithiques), mais ces lieux sont tristes et
appelés à disparaître. L’un des points intéressant ici est le
modèle économique associé à cette utilisation côté utilisateur
et son impacte sur le futur de cet écosystème.
L’autre aspect qui m’intéresse est la façon dont ce même eco-système va
(di)gérer l’accélération des cycles de vie de ces nouvelles applications.
Mais ces questions et quelques autres comme l’adaptabilité à
la transformation digitale des grosses structures organisationnelles
seront l’objet de *prochains articles*.

###### Pour l’heure la question est “Quel CMS pour FMJConsulting?”
Il faut qu’il soit simple, rapide, un minimum pérène, ne nécessitant
pas trop de développements pour être viable et compatible avec
les VPS standard du marché. Son installation et les manipulations associés
pour y ajouter quelques futurs plugins ne doivent pas non plus
être synonyme de nuits blanches.
Et il faut qu’il soit sobrement esthétique.
C’est à dire que je dois pouvoir y écrire des billets contenant
des exemples de code,
insérer des captures d’écran ou des images,
mettre en page des textes de façon professionnel sans m’arracher
les quelques cheveux qui me restent. Comme mentionné précédement j’ai donc
créé deux pages type que j’ai soumis aux quelques framework/CMS que j’ai testé.
Une utilisant un layout me permettant d’afficher l’article que vous lisez.
L’autre reprenant l’ensemble des éléments visuels que je souhaite
avoir à ma disposition comme les titres, bullet, insertion de code
et d’image etc, en un seul lieu. De tel sorte qu'il me soit possible de juger
de l'aspect esthetique global d'un seul coup d'oeil.


Name        | installed | Tested | Status | |Name | installed | Tested | Status
------------|-----------|--------|--------|-|-----|-----------|--------|--------
Ghost       | Yes       |  Yes   |Approved| |ExpressJS| Yes   |Yes | Approved
KeystoneJS  |           |        |        | |total.js| | |
Hatchjs     |           |        |        | |cody.js| | |
Croogo      |           |        |        | |nodize CMS| | |
CakePHP     |           |        |        | |Hexo.io| | |
subrion     |           |        |        | |Scotch| | |
snipcart    |           |        |        | |InvaNode| | |
Typo3       |           |        |        | |taracot| | |
concrete5   |           |        |        | |butterCMS| n/a | No | Approved
html5up     |           |        |        | |bucket| No| No | Failed to install
**Grav**    |           |        |        | |AsgardCMS| | |
MadeSimple  |           |        |        | |October| | |
SPIP        |           |        |        | |lavalite| | |
Zwii        |           |        |        | |botble| No| No | notfree
WebGUI      |           |        |        | |quarx| No| No| Failed to install
eZ Publish  |           |        |        | |pyrocms| | |
ghost       |           |        |        | |thinmartian| | |
enduroJS    |           |        |        | |typicms| | |
callipso    |           |        |        | |coastercms|Yes |Yes | Approved
pencilblue  |           |        |        | |quarx| | |
apostrophe2 |           |        |        | |Simpla| | |
Assemble    |           |        |        | |Bootstrap| | |
Amber.js    |           |        |        | |upstage| | |
PageKit     |           |        |        |
Pour toutes ces raisons je garde une short list avec GRAV, KEYSTONEJS

[¹] Ce qui abouti tout de même à une bonne cinquantaine d'unités applicative à tester.
[1] DSL: Domaine Specific Language - [Domain-specific_language](https://en.wikipedia.org/wiki/Domain-specific_language)

[2] Pour beaucoup d’autres qui expliquent une façon (parfois) légerement différente d’installer l'application, selon que l’on consulte le site web, github, une Doc pdf, la FAQ.

3 https://www.opensourcecms.com/general/cms-marketshare.php

4 La bonne démarche serait de s’inspirer d’autres sites qui me semblent
 correspondre à ce que je recherche et de m'en inspirer. mais le manque de temps et
 de réflexes lorsque j’en croise un ont eu raison de cette bonne idée.
 C’est un tord que je vais m’empresser de rectifier.

 D'autant qu'il n’est pas difficile d’en trouver quelques uns de très bon niveau :
 * http://dev.glicer.com/
 * https://davidwalsh.name/
 * http://www.phpied.com/
 * https://blog.codinghorror.com/
 * https://alexsexton.com/
 * https://www.paulirish.com/
 * https://scotch.io/
 * https://www.smashingmagazine.com/

 Et à l’heure où j’écris ces lignes il est possible d’en trouver
 une bonne synthèse sur le site: [securityinnovationeurope](https://www.securityinnovationeurope.com/blog/page/40-blogs-every-software-developer-should-be-reading).
 Mais cela est un autre sujet.

 [⁴] Jetez un coup d’oeil à
   * http://passportjs.org/
   * https://www.hotjar.com
   * https://www.polymer-project.org/
5 -Ils sont assez bluffant: Des ghists pour générer entierement votre blog!-
